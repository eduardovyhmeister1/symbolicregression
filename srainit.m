function sra = srainit(sra)
%% Symbolic Regression Alamo by Eduardo Vyhmeister. V 1.19.08.2020
if ~sra.run.equation_sys && ~sra.run.PDE && ~sra.run.ODE
    vars=sra.data.num_inp;
elseif sra.run.equation_sys || sra.run.ODE
    vars=sra.data.num_inp+sra.data.num_output;
elseif sra.run.PDE
    vars=sra.data.num_inp+sra.data.num_output+1+size(sra.data.axes,2)*sra.PDE.derivorder; %
end
if strcmp(sra.run.algorithm,'sr')
    alphabet={'a','b','c','d','f','g','h','k','l','m','n','o','p','q','r','s','t','u','v','w','y','z','A','B','C','D','F','G','H','K','L','M','N','O','P','Q','R','S','T','U','V','W','Y','Z'};
    for i = 1:length(sra.sr.functions.name)   
        arity = nargin(sra.sr.functions.name{i});
        if arity == -1
            error(['The function ' sra.sr.functions.name{i} ' may not be used (directly) as a function node because it has a variable number of arguments.']);
        end 
        sra.sr.functions.arity(i) = arity;
    end  
    sra.sr.functions.afid = horzcat(alphabet{1:length(sra.sr.functions.name)});
    fun_argt0 = sra.sr.functions.arity > 0;
    fun_areq0 =~ fun_argt0;
    sra.sr.functions.afid_argt0 = sra.sr.functions.afid(fun_argt0); %functions with arity > 0
    sra.sr.functions.afid_areq0 = sra.sr.functions.afid(fun_areq0); %functions with arity == 0
    sra.sr.functions.arity_argt0 = sra.sr.functions.arity(fun_argt0);
    %sra.sr.functions.fun_lengthargt0 = numel(sra.sr.functions.afid_argt0);
    %sra.sr.functions.fun_lengthareq0 = numel(sra.sr.functions.afid_areq0);
    if  vars == 0 && sra.sr.const.p_ERC == 0 && ...
            isempty(find(sra.sr.functions.arity(logical(sra.sr.functions.active)) == 0, 1))
        error('No terminals (inputs, constants or zero arity functions) have been defined for this run.');
    end
else
    sra.pop={};
    F={};
     if sra.ala.use_user_functions==0 || isempty(sra.ala.use_user_functions)==1  
        if ~isempty(sra.ala.mono)
            sra.ala.functions{end+1}='€.^%';
            sra.ala.functions_powers{end+1}=sra.ala.mono;
        end
        if ~isempty(sra.ala.multi2)
            sra.ala.functions{end+1}='€.^%*.€.^%';
            sra.ala.functions_powers{end+1}=sra.ala.multi2;
        end
        if ~isempty(sra.ala.multi3)
            sra.ala.functions{end+1}='€.^%*.€.^%*.€.^%';
            sra.ala.functions_powers{end+1}=sra.ala.multi3;
        end
        if ~isempty(sra.ala.lnfcns)
            sra.ala.functions{end+1}='log(€.^%)';
            sra.ala.functions_powers{end+1}=sra.ala.lnfcns;
        end
        if ~isempty(sra.ala.logfcns)
            sra.ala.functions{end+1}='log10(€.^%)';
            sra.ala.functions_powers{end+1}=sra.ala.logfcns;
        end
        if ~isempty(sra.ala.expfcns)
            sra.ala.functions{end+1}='exp(€.^%)';
            sra.ala.functions_powers{end+1}=sra.ala.expfcns;
        end
        if ~isempty(sra.ala.sinfcns)
            sra.ala.functions{end+1}='sin(€.^%)';
            sra.ala.functions_powers{end+1}=sra.ala.sinfcns;
        end
        if ~isempty(sra.ala.cosfcns)
            sra.ala.functions{end+1}='cos(€.^%)';
            sra.ala.functions_powers{end+1}=sra.ala.cosfcns;
        end
     else   
        sra.ala.functions=sra.ala.user_functions;
        sra.ala.functions_powers=sra.ala.user_functions_powers;
        sra.ala.functions_gama=sra.ala.user_functions_gama;
     end
    for i=1:numel(sra.ala.functions)
        function_out={};
        function_in=sra.ala.functions{i};
        var_num=strfind(function_in,'€'); %find the locations of the variables
        pow_num=strfind(function_in,'%'); %find the locations of the powers
        const_num=strfind(function_in,'£'); %find the location of the constants
        for zz=1:sra.data.num_output 
            if numel(var_num)>0
               varst=1:vars;
               if sra.run.equation_sys 
                   varst=vars(vars~=zz);
               end
               permutations = permn(varst,numel(var_num));                   
                for j=1:size(permutations,1)
                    function_m=function_in;
                    for k=1:numel(var_num)
                       first=strfind(function_m,'€');
                       function_m = [function_m(1:first(1) - 1) 'x' int2str(permutations(j,k)) function_in(first(1) + 1:end)];
                    end
                   function_out{end+1}=function_m; 
                end              
            else
                function_out={function_in};
            end
            
            if numel(pow_num)>0
                function_out2={};
                permutations = permn(sra.ala.functions_powers{i},numel(pow_num));
                powers=cellfun(@num2str,(num2cell(permutations)),'UniformOutput',false);
                for l=1:numel(function_out)
                    for j=1:size(permutations,1)
                        for k=1:size(permutations,2)
                            first=strfind(function_out,'%')
                            moen=cellfun(@(x)strrep(x,'%',powers{j}),function_out,'UniformOutput',false); 
                            function_out2=[function_out2, moen];
                        end
                    end
                end
            elseif numel(var_num)==0 && numel(pow_num)==0
                function_out2={function_in};
            elseif numel(pow_num)==0
                function_out2=function_out;
            end
            if numel(const_num)>0
                function_out3={};
                permutations = permn(sra.ala.functions_gama{i},numel(const_num));
                gamas=cellfun(@num2str,(num2cell(permutations)),'UniformOutput',false);
                for j=1:length(gamas)
                    moen=cellfun(@(x)strrep(x,'£',gamas{j}),function_out2,'UniformOutput',false); 
                    function_out3=[function_out3, moen];
                end
            elseif numel(const_num)==0 && numel(pow_num)==0 && numel(var_num)==0
                function_out3={function_in};
            elseif numel(const_num)==0 && numel(pow_num)==0 
                function_out3=function_out1;
            elseif numel(const_num)==0
                function_out3=function_out2;
            end
            if isempty(var_num) && isempty(pow_num) && isempty(const_num)
                function_out3=sra.ala.functions{i};
            end
            
            if isempty(sra.pop)
                sra.pop{1}=[function_out3];
            else
               sra.pop{zz}=[sra.pop{zz} function_out3];
            end
            function_out={};
            function_out2={};
            function_out3={};
        end
    end   
    if sra.run.equation_sys
        xdata=horzcat(sra.data.y,sra.data.x);
    elseif sra.run.ODE
        xdata=hozcat(sra.data.y,sra.data.x,sra.data.t);
    elseif sra.run.PDE
        xdata=hozcat(sra.data.y,sra.data.x,sra.data.axes,sra.data.t);  
    else
        xdata=sra.data.x;
    end
    
    for zz=1:sra.data.num_output
        toeliminate=[];
        for ll=1:numel(sra.pop{zz})   
            eval(['out=' regexprep(sra.pop{zz}{ll},'x(\d+)','xdata(:,$1)') ';'])
            if any(~isreal(out)) || any(isnan(out)) || any(isinf(out))
                disp(['The kernel ' sra.pop{zz}{ll} ' produces infinities, imaginary or NaN values...eliminated'])
                toeliminate=[toeliminate ll]; %eliminate any kernel that produce an anstable point
            end
        end
        sra.pop{zz}(toeliminate)=[];
        sra.beta_pop{zz} = cellfun(@(x,y)['(B(' x ')*' y ')'],cellstr(string(1:numel(sra.pop{zz}))),sra.pop{zz},'UniformOutput',false);
        if isempty(sra.run.user_fcn)==0
            for ll=1:numel(sra.run.user_fcn)
                sra.beta_pop{zz}=[sra.beta_pop{zz} sra.run.user_fcn{ll}];
            end
        end
        sra.eval_pop{zz} = regexprep(sra.beta_pop{zz},'x(\d+)','xdata(:,$1)');

        if numel(sra.ala.lb)~=numel(sra.pop{zz}) || numel(sra.ala.ub)~=numel(sra.pop{zz})
            sra.ala.lb=-inf*ones(1,numel(sra.pop{zz}));
            sra.ala.ub=inf*ones(1,numel(sra.pop{zz}));
            disp(['The lower and upper limits sra.ala.lb sra.ala.ub are not correctly set. the number of functions its: ' num2str(numel(sra.pop))])
            disp('We have set the limits of each element between -inf and inf')
            disp('Cancel run if needed and modify...pause for 5 seconds')
            pause(5)
        end

        %construct evaluative handle

        F{zz}='';
        numfunc=numel(sra.eval_pop{zz});
        for d=1:numfunc-1 %create the function for OLS 
                F{zz}=[F{zz} sra.eval_pop{d} '+'];
        end
        fullset{zz}=[F{zz} sra.eval_pop{end}];
        F{zz}=str2func(['@(B,xdata)' fullset{zz}]); %create the handle
    end
    sra.function=fullset;
    sra.function_handle = F;  
end

disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
disp('Runing SRA code');
disp('by Eduardo Vyhmeister');
disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
if sra.run.equation_sys
    disp('Type of System:               Equation');
elseif sra.run.PDE
    disp('Type of System:         Partial Differential'); 
elseif sra.run.ODE
    disp('Type of System:         Ordinary Differential');    
else
    disp('Type of System:         Individual Equation');   
end
if strcmp(sra.run.algorithm,'sr')
    disp('Algorithm:               Symbolic Regression');
    disp(['Population size:         ' int2str(sra.run.pop_size)]);
    disp(['Number of generations:   ' int2str(sra.run.num_gen)]);
    disp(['Tournament size:         ' int2str(sra.sr.tournament.size)]);
    disp(['Elite fraction:          ' num2str(sra.sr.elite_fraction)]);  
    disp(['Max gen depth:           ' int2str(sra.sr.max_depth)]);
    disp(['Max nodes per gen:       ' int2str(sra.sr.max_nodes)]);
    a=join(cellfun(@(x)horzcat(x,' '),sra.sr.functions.name,'UniformOutput',false));
    disp(['Using function set:     ' a{1}]);
    disp(['Number of inputs:        ' int2str(sra.data.num_inp)]);
    disp(['Max genes:               ' int2str(sra.sr.max_genes)]);
    if ~sra.sr.const.p_ERC
        disp('Using no constants');
    else
        disp(['Constants range:         [' num2str(sra.sr.const.range) ']']);
    end   
    if sra.run.complexityMeasure
        disp('Complexity measure:      expressional');
    else
        disp('Complexity measure:      node count');
    end
    disp(['Fitness function:        ' func2str(sra.run.fitfun) '.m']);
    disp(' ');
    
elseif strcmp(sra.run.algorithm,'ala')
    disp('Algorithm:               Alamo');
    disp(['Iteration processes:         ' int2str(sra.run.pop_size)]);
    disp(['Number of kernels:               ' int2str(numel(sra.pop{1}))])
    disp(['The modeler used is:               ' sra.ala.modeler])

    disp(['The user forced functions is active?:               ' num2str(isempty(sra.run.user_fcn))])
    disp(['Equation forced to be added:' ])
    for j=1:numel(sra.run.user_fcn)
        disp(['Function Number (' int2str(j) ') :' sra.run.user_fcn{j} ])
    end
    if sra.ala.use_user_functions==1
        disp('The user functions kernels is active?:                yes')
    else
        disp('The user functions kernels is active?:                no')   
    end
    if strcmp(sra.ala.optimizer,'ga')==1
        disp('optimizer:                Genetic Algorithm')
    elseif strcmp(sra.ala.optimizer,'baron')==1
        disp('Optimizaer:               Baron')
    elseif strcmp(sra.ala.optimizer,'user')==1
        disp('Optimizaer:               User defined at sra.ala.user_optimizer')
    elseif strcmp(sra.ala.optimizer,'MINLP')==1
        disp('Optimizaer:               MINLP')    
    else
        error('The optimizer is incorrectly set')
    end
end

%log run start time
sra.info.startTime = datestr(now,0);

%set elapsed run time count to zero seconds
sra.state.runTimeElapsed = 0;

