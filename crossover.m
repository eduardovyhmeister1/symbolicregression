function [s,d] = crossover(mum,dad,sra)
%select random crossover nodes in mum and dad expressions
m_position = picknode(mum,0,sra);
d_position = picknode(dad,0,sra);
%extract main and subtree expressions and combine
[m_main,m_sub] = extract(m_position,mum);
[d_main,d_sub] = extract(d_position,dad);
d = strrep(m_main,'€',d_sub);
s = strrep(d_main,'€',m_sub);
